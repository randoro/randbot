﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RandBot.Actions
{
    class ClickLocationAction : ScriptAction
    {
        public ClickType type;
        public Point location;

        public ClickLocationAction(ClickType _type, Point _location, int _afterSleep)
        {
            type = _type;
            afterSleep = _afterSleep;
            location = _location;
        }

        public override void DoAction()
        {
            

            switch (type)
            {
                case ClickType.Left:

                    FakeMouse.SetCursorPos(location);
                    FakeMouse.SendMouseLeftClick();
                    break;
                case ClickType.Right:

                    FakeMouse.SetCursorPos(location);
                    FakeMouse.SendMouseRightClick();
                    break;
                default:
                    break;

            }


            Thread.Sleep(afterSleep);
        }
    }
}
