﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;

namespace RandBot.Actions
{
    class KeyBoardActionHold : ScriptAction
    {
        public VirtualKeyCode keyCode;

        public KeyBoardActionHold(VirtualKeyCode _keyCode, int _afterSleep)
        {
            afterSleep = _afterSleep;
            keyCode = _keyCode;


        }

        public override void DoAction()
        {
            InputSimulator.SimulateKeyDown(keyCode);

            Thread.Sleep(afterSleep);
        }
    }
}
