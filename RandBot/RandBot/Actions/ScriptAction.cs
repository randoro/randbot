﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandBot.Actions
{
    abstract class ScriptAction
    {

        public int afterSleep = 100;

        public abstract void DoAction();
    }
}
