﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RandBot.Actions
{
    class MoveMouseAction : ScriptAction
    {
        public int xPos;
        public int yPos;

        public MoveMouseAction(int _xPos, int _yPos, int _afterSleep)
        {
            afterSleep = _afterSleep;
            xPos = _xPos;
            yPos = _yPos;

        }

        public override void DoAction()
        {
            FakeMouse.MoveMouse(xPos, yPos);

            Thread.Sleep(afterSleep);
        }
    }
}
