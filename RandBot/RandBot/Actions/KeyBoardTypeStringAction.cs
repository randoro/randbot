﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;

namespace RandBot.Actions
{


    class KeyBoardTypeStringAction : ScriptAction
    {
        public String keys;

        public KeyBoardTypeStringAction(String _keys, int _afterSleep)
        {
            afterSleep = _afterSleep;
            keys = _keys;


        }

        public override void DoAction()
        {
            char[] chars = keys.ToCharArray();
            VirtualKeyCode[] vks = new VirtualKeyCode[chars.Length];

            for (int i = 0; i < chars.Length; i++)
            {
                if (!FakeKeyboard.GetVirtualKeyCode(chars[i], out vks[i]))
                {
                    return;
                }
            }

            for (int i = 0; i < vks.Length; i++)
            {
                InputSimulator.SimulateKeyPress(vks[i]);
                Thread.Sleep(50);
            }
            

            Thread.Sleep(afterSleep);
        }
    }
}
