﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RandBot.Scripts;

namespace RandBot.Actions
{
    class CheckLineCrossAction : ScriptAction
    {
        public int times;

        public CheckLineCrossAction(int _times, int _afterSleep)
        {
            afterSleep = _afterSleep;
            times = _times;


        }

        public override void DoAction()
        {

            Point p = ScriptControl.lineCross;
            ClickAction ca = new ClickAction(ClickType.Right, 100);

            if (p.X == -1 && p.Y == -1)
            {
                Logger.printError("Cant find fishing line. Rethrowing..");
                ca.DoAction();
                return;
            }


            for (int i = 0; i < times; i++)
            {
                ScreenReader.RefreshScreenMap();
                if (ScreenReader.CheckLineCrossCatch(p.X, p.Y, 10))
                {
                    ca.DoAction();
                    return;
                }
            }

            ca.DoAction();

            Thread.Sleep(afterSleep);
        }
    }
}
