﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;

namespace RandBot.Actions
{
    class KeyBoardActionRelease : ScriptAction
    {
        public VirtualKeyCode keyCode;

        public KeyBoardActionRelease(VirtualKeyCode _keyCode, int _afterSleep)
        {
            afterSleep = _afterSleep;
            keyCode = _keyCode;


        }

        public override void DoAction()
        {
            InputSimulator.SimulateKeyUp(keyCode);

            Thread.Sleep(afterSleep);
        }
    }
}
