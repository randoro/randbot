﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RandBot.Scripts;

namespace RandBot.Actions
{
    class FindAndCreateLineCrossAction : ScriptAction
    {
        public int pathToCheck;

        public FindAndCreateLineCrossAction(int _pathToCheck, int _afterSleep)
        {
            afterSleep = _afterSleep;
            pathToCheck = _pathToCheck;


        }

        public override void DoAction()
        {
            ScreenReader.RefreshScreenMap();
            Point p = ScreenReader.GetLineCross(640, 411, pathToCheck);
            ScriptControl.lineCross = p;

            Thread.Sleep(afterSleep);
        }
    }
}
