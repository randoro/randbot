﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using WindowsInput;

namespace RandBot.Actions
{
    class HoldAction : ScriptAction
    {
        public ClickType type;
        public int time;

        public HoldAction(ClickType _type, int _time, int _afterSleep)
        {
            type = _type;
            afterSleep = _afterSleep;
            time = _time;


        }

        public override void DoAction()
        {
            switch (type)
            {
                case ClickType.Left:

                    FakeMouse.SendMouseLeftHold(time);
                    break;
                case ClickType.Right:

                    FakeMouse.SendMouseRightHold(time);
                    break;
                default:
                    break;

            }


            Thread.Sleep(afterSleep);
        }
    }
}
