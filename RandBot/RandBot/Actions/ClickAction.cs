﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;

namespace RandBot.Actions
{

    public enum ClickType { Left, Right }

    class ClickAction : ScriptAction
    {
        public ClickType type;
        public ClickAction(ClickType _type, int _afterSleep)
        {
            type = _type;
            afterSleep = _afterSleep;
        }

        public override void DoAction()
        {
            

            switch (type)
            {
                case ClickType.Left:

                    FakeMouse.SendMouseLeftClick();
                    break;
                case ClickType.Right:

                    FakeMouse.SendMouseRightClick();
                    break;
                default:
                    break;

            }


            Thread.Sleep(afterSleep);
        }
    }
}
