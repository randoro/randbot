﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using RandBot.Scripts;

namespace RandBot.Actions
{
    class ResetAndFinishAction : ScriptAction
    {

        public ResetAndFinishAction(int _afterSleep)
        {
            afterSleep = _afterSleep;


        }

        public override void DoAction()
        {
            //ScriptControl.GetCurrentScript().ResetScript();

            //InputSimulator.SimulateKeyPress(VirtualKeyCode.NUMLOCK);

            Thread.Sleep(afterSleep);
        }
    }
}
