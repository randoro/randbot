﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using RandBot;
using RandBot.Scripts;

namespace RandBot
{
    static class CommandControl
    {

        public enum Commands { commands, createauthfile, fixwindow, help, savecurrentscript, setdetections, load, getmouse }
        public enum LoadArguments { }

        private static String[] getElements(String[] commandElements)
        {
            if (commandElements.Length > 1)
            {
                String[] elements = new String[commandElements.Length - 1];
                Array.Copy(commandElements, 1, elements, 0, commandElements.Length - 1);
                return elements;
            }
            else
            {
                return null;
            }
        }

        public static void ListenForCommand()
        {
            String command = Console.ReadLine();
            String[] commandElements = command.Split(' ');

            String[] elements = getElements(commandElements);
            try
            {
                Commands commandEnum = (Commands) Enum.Parse(typeof (Commands), commandElements[0]);


                switch (commandEnum)
                {
                    case Commands.commands:
                        Logger.print("Available commands:");
                        PrintCommands();
                        break;
                    case Commands.createauthfile:
                        if (Authenticator.canCreateAuthFile)
                        {
                            Program.auth.CreateAuthFile();
                        }
                        break;
                    case Commands.fixwindow:

                        if (elements == null)
                        {
                            ScreenReader.FixWindowsPlacement();
                        }
                        else
                        {
                            Logger.print("Unknown command parameters. Type 'help' for help.");
                        }
                        break;
                    case Commands.getmouse:
                            Logger.print("Writing mouse location in 5 seconds");
                            Thread.Sleep(5000);

                        for (int i = 0; i < 100; i++)
                        {
                            Point p = FakeMouse.GetCursorPos();
                            ScreenReader.RefreshScreenMap();
                            Color color = ScreenReader.GetPixelColor(p.X, p.Y);


                            Logger.print("Mouse X:" + p.X + " Y:" + p.Y);
                            Logger.print("Mouse R:" + color.R + " G:" + color.G + " B:" + color.B);
                            Thread.Sleep(5);
                        }
                            
                        break;
                    case Commands.savecurrentscript:

                        if (elements == null)
                        {
                            Logger.print("Specify a name for your script.");
                        }
                        else
                        {
                            try
                            {
                                if (Regex.IsMatch(elements[0], @"^\d+"))
                                {
                                    Logger.printError("Filename can't start with a number.");
                                    break;
                                }

                                FileManager.SaveScript(elements[0], ScriptControl.GetCurrentScript());
                                Logger.print("Script " + elements[0] + " saved to the Script folder.");
                            }
                            catch (ArgumentException e)
                            {
                                Logger.printError("Invalid characters in filename.");
                            }
                        }
                        break;
                    case Commands.setdetections:
                        ScriptControl.GetCurrentScript().SetDetection();

                        break;
                    case Commands.help:

                        HelpSection(elements);
                        break;
                    case Commands.load:

                        if (elements == null)
                        {
                            Logger.print("Specify a name for the script to load.");
                        }
                        else
                        {
                            try
                            {
                                if (Regex.IsMatch(elements[0], @"^\d+"))
                                {
                                    Logger.printError("Filename can't start with a number.");
                                    break;
                                }

                                Script scr = FileManager.LoadScript(elements[0]);

                                if (scr != null)
                                {
                                    ScriptControl.SetCurrentScript(scr);
                                    FileManager.SaveGlobalSettings();
                                    Logger.print("Script " + elements[0] + " loaded.");
                                }
                                
                            }
                            catch (ArgumentException e)
                            {
                                Logger.printError("Invalid characters in filename.");
                            }
                        }
                        break;
                    default:
                        Logger.printError("There is no case for enum value " + commandEnum);
                        break;
                }
            }
            catch (ArgumentException e)
            {
                Logger.print("Unknown command. Type 'help' for help.");
            }
        }

        private static void PrintCommands()
        {
            foreach (Commands foo in Enum.GetValues(typeof(Commands)))
            {
                Logger.print(foo.ToString());
            }
        }

        public static void HelpSection(String[] elements)
        {
            try
            {
                if (elements != null)
                {
                    CommandControl.Commands commandEnum = (CommandControl.Commands) Enum.Parse(typeof (CommandControl.Commands), elements[0]);


                    switch (commandEnum)
                    {
                        case CommandControl.Commands.commands:
                            Logger.print("Shows a list of all available commands.");
                            break;
                        case CommandControl.Commands.fixwindow:
                            Logger.print(
                                "Fixes both Craftland window and Randbot window placement to align with pixel placements.");
                            break;
                        case CommandControl.Commands.help:
                            Logger.print("Shows this help page, duh..");
                            break;
                        default:
                            Logger.printError("Theres no case for this in helper section!");
                            break;
                    }
                }
                else
                {
                    Logger.print("Available parameters for help command:");
                    PrintCommands();
                }
            }
            catch (ArgumentException e)
            {
                Logger.print("Unknown command. Type 'help' for help.");
            }
        }


    }
}
