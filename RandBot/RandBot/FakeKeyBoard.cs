﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WindowsInput;

namespace RandBot
{
    static class FakeKeyboard
    {

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        static extern short VkKeyScanEx(char ch, IntPtr dwhkl);

        [DllImport("user32.dll")]
        static extern bool UnloadKeyboardLayout(IntPtr hkl);

        [DllImport("user32.dll")]
        static extern IntPtr LoadKeyboardLayout(string pwszKLID, uint Flags);

        private static readonly IntPtr pointer = LoadKeyboardLayout("00000409", 1);

        public static bool GetVirtualKeyCode(char character, out VirtualKeyCode value)
        {
            short keyNumber = VkKeyScanEx(character, pointer);
            if (keyNumber == -1)
            {
                value = VirtualKeyCode.ESCAPE;
                return false;
            }
            value = (VirtualKeyCode)keyNumber;
            return true;
        }
    }
}
