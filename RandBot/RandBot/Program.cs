﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using WindowsInput;
using Newtonsoft.Json;
using RandBot.Actions;
using RandBot.Scripts;

namespace RandBot
{
    [StructLayout(LayoutKind.Sequential)]
    public struct Point
    {
        public int X;
        public int Y;

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }



    class Program
    {

        //This is a replacement for Cursor.Position in WinForms
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetCursorPos(int x, int y);


        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetCursorPos(out Point lpPoint);

        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(int hProcess,
          int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern int ToUnicode(
            uint virtualKeyCode,
            uint scanCode,
            byte[] keyboardState,
            StringBuilder receivingBuffer,
            int bufferSize,
            uint flags
        );

        [DllImport("kernel32.dll")]
        public static extern IntPtr GetModuleHandle(string lpModuleName);

        const int PROCESS_WM_READ = 0x0010;

        

        static bool keystate1;
        static bool oldKeystate1;
        static bool paintscreenshot;
        static bool keystate2;
        static bool oldKeystate2;
        static private int movementLeft;
        static private bool screenbool;

        public static GlobalSettings globalSettings;


        public static Authenticator auth;


        static void Main(string[] args)
        {
#if DEBUG
            ScriptGenerator.GenerateScript();
            ScriptGenerator.GenerateCraftScript();
            ScriptGenerator.GenerateFishingScript();
#endif

            Console.Title = "Randbot";
            Logger.print("RandBot 1.013 started.");
            Logger.print("Searching for authentication file..");



            auth = new Authenticator();
            //FileManager.LoadScript();

            //FileManager.SaveScript("TestScript1");

            if (!auth.Login())
            {
                Logger.printError("Login failed... Application will close in 10 seconds.");
                Thread.Sleep(10000);
                Environment.Exit(0);
            }


            if (!InputSimulator.IsTogglingKeyInEffect(VirtualKeyCode.NUMLOCK))
            {
                Logger.print("Application detected that your numlock key was off, and activated it.");
                InputSimulator.SimulateKeyPress(VirtualKeyCode.NUMLOCK);
            }

            Logger.print("Use Numlock to toggle script on and off.");

            globalSettings = FileManager.LoadGlobalSettings();

            ScriptControl.InitilizeScript();

            //Console.ReadLine();

            //ScriptControl.DeactivateScript();

            //while (true)
            //{
                Thread.Sleep(10);
                //String command = Console.ReadLine();
            while (true)
            {
                CommandControl.ListenForCommand();
            }

            //keystate1 = (!InputSimulator.IsTogglingKeyInEffect(VirtualKeyCode.NUMLOCK));
            //if ((keystate1 && !oldKeystate1))
            //{
            //    keystate1 = false;

            //}
            //oldKeystate1 = keystate1;



            //if (InputSimulator.IsKeyDown(VirtualKeyCode.CONTROL) && InputSimulator.IsKeyDown(VirtualKeyCode.VK_1))
            //if (command.Equals("fixwindow"))
            //{
            //    ScreenReader.FixWindowsPlacement();

            //}
            //else if (command.Equals("saveglobal"))
            //{
            //    FileManager.SaveGlobalSettings();
            //}


            //if (InputSimulator.IsKeyDown(VirtualKeyCode.CONTROL) && InputSimulator.IsKeyDown(VirtualKeyCode.VK_2))
            //{
            //    ScriptControl.script.ResetDetection();

            //}

            //if (InputSimulator.IsKeyDown(VirtualKeyCode.CONTROL) && InputSimulator.IsKeyDown(VirtualKeyCode.VK_F))
            //{

            //    ScreenReader.RefreshScreenMap();

            //    Point p;
            //    if (GetCursorPos(out p))
            //    {
            //        Color c = Color.FromArgb(0, 0, 0);
            //        c = ScreenReader.GetPixelColor(p.X, p.Y);
            //        Logger.print("Pos " + p.X + ":" + p.Y + "   R:" + c.R + "   G:" + c.G + "   B:" + c.B);
            //    }

            //}
        //}
            
        }


    }
}
