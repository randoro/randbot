﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RandBot.Scripts;

namespace RandBot
{
    static class FileManager
    {
        public static String BaseDirectory = AppDomain.CurrentDomain.BaseDirectory;
        public static String BasePlusScriptDirectory = BaseDirectory + "Scripts";

        public static GlobalSettings LoadGlobalSettings()
        {
            Logger.print(BaseDirectory);
            if (File.Exists(BaseDirectory + "GlobalSettings.json"))
            {
                String text = File.ReadAllText(BaseDirectory + "GlobalSettings.json");
                GlobalSettings newSettings = JsonConvert.DeserializeObject<GlobalSettings>(text);
                return newSettings;
            }
            return new GlobalSettings();


        }


        public static void SaveGlobalSettings()
        {
            Directory.CreateDirectory(BasePlusScriptDirectory);

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };
            String str = JsonConvert.SerializeObject(Program.globalSettings, settings);
            //Logger.print("Json: " + str);

            File.WriteAllText(BaseDirectory + "GlobalSettings.json", str);

        }



        public static String[] LoadScriptsPaths()
        {
            Directory.CreateDirectory(BasePlusScriptDirectory);
            String[] files = Directory.GetFiles(BasePlusScriptDirectory, "*.json",
                SearchOption.AllDirectories);//.Select(f => Path.GetFileName(f));

            //for (int i = 0; i < files.Count(); i++)
            //{
            //    Logger.print(files.ElementAt(i));
            //}
            return files;

            //for (int i = 0; i < files.Length; i++)
            //{
            //    Script json = JsonConvert.DeserializeObject<Script>(files[i]);
            //}

        }


        public static Script LoadScriptWithPath(String scriptPathAndName)
        {
            Directory.CreateDirectory(BasePlusScriptDirectory);
            if (File.Exists(scriptPathAndName))
            {
                String text = File.ReadAllText(scriptPathAndName);

                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                };
                Script json = JsonConvert.DeserializeObject<Script>(text, settings);
                return json;
            }
            return null;
        }

        public static Script LoadScript(String scriptName)
        {
            Directory.CreateDirectory(BasePlusScriptDirectory);
            if (File.Exists(BasePlusScriptDirectory + "\\"+ scriptName +".json"))
            {
                String text = File.ReadAllText(BasePlusScriptDirectory + "\\" + scriptName + ".json");

                JsonSerializerSettings settings = new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All
                };
                Script json = JsonConvert.DeserializeObject<Script>(text, settings);
                return json;
            }
            return null;
        }

        public static void SaveScriptWithPath(String scriptPathAndName)
        {
            Directory.CreateDirectory(BasePlusScriptDirectory);

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };
            String str = JsonConvert.SerializeObject(ScriptControl.GetCurrentScript(), settings);
            //Logger.print("Json: " + str);

            File.WriteAllText(scriptPathAndName, str);
            

        }

        public static void SaveScript(String scriptName, Script script)
        {
            Directory.CreateDirectory(BasePlusScriptDirectory);

            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            };
            String str = JsonConvert.SerializeObject(script, settings);
            //Logger.print("Json: " + str);

            File.WriteAllText(BasePlusScriptDirectory + "\\"+ scriptName +".json", str);


        }

    }
}
