﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace RandBot
{
    class Authenticator
    {
        private string exeLocation;
        private string directory;
        private string authFile;

        private byte[] key = {9, 3, 0, 1, 1, 2, 2, 3, 6, 7, 5, 3, 5, 7, 8, 2, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 6, 4, 3, 2, 3, 2};

        private byte[] IV = { 0, 3, 4, 3, 3, 4, 9, 2, 8, 6, 5, 2, 6, 4, 1, 1 };

        public static bool canCreateAuthFile = false;

        public Authenticator()
        {
            exeLocation = System.Reflection.Assembly.GetExecutingAssembly().Location;
            directory = System.IO.Path.GetDirectoryName(exeLocation);
            authFile = directory + "\\auth.txt";

        }



        public bool Login()
        {
            Logger.print("Please enter your username:");
            string username = Console.ReadLine();
            if (username.Equals("randoro"))
            {
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    return true;
                }


                string pass = "";
                Logger.print("Enter your password: ");
                ConsoleKeyInfo key;

                do
                {
                    key = Console.ReadKey(true);

                    // Backspace Should Not Work
                    if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                    {
                        pass += key.KeyChar;
                        Console.Write("*");
                    }
                    else
                    {
                        if (key.Key == ConsoleKey.Backspace && pass.Length > 0)
                        {
                            pass = pass.Substring(0, (pass.Length - 1));
                            Console.Write("\b \b");
                        }
                    }
                }
                // Stops Receving Keys Once Enter is Pressed
                while (key.Key != ConsoleKey.Enter);

                Console.WriteLine();

                if (pass.Equals("snoppis"))
                {
                    Logger.print("Welcome randoro.");

                    canCreateAuthFile = true;

                    return true;
                }
                else
                {
                    Logger.printError("You're not Randoro.");
                    return false;
                }
            }
            else
            {

                if (!ScreenReader.AuthWindow(username))
                {
                    return false;
                }

                return AuthUsingFile(username);
            }

            return false;
        }


        public bool AuthUsingFile(String username)
        {
            if (!System.IO.File.Exists(authFile))
            {
                Logger.print("Can't find Auth file, it should be located:" + authFile);
            }



            using (AesManaged myAes = new AesManaged())
            {

                byte[] outer = System.IO.File.ReadAllBytes(authFile);
                string decrypted = AesControl.DecryptStringFromBytes_Aes(outer, key, IV);
                string[] argumentArray = decrypted.Split(',');



                if (!argumentArray[0].Equals(username))
                {
                    Logger.printError("Wrong username/authfile combination");
                    return false;
                }

                try
                {


                    string binary = argumentArray[1];


                    long binaryLong = Int64.Parse(binary);

                    DateTime then = DateTime.FromBinary(binaryLong);
                    DateTime now = DateTimeOnline.GetNistTime();

                    if (now.Date > then.Date)
                    {
                        Logger.printError("Authfile has expired.");
                        return false;
                    }
                    else
                    {
                        Logger.print("Authfile OK! Expires:" + then.Date);
                        return true;
                    }


                }
                catch (Exception ex)
                {
                    Logger.printError("Corrupted authfile");
                    return false;
                }


            }


            return false;
        }



        public void CreateAuthFile()
        {
            try
            {
                Logger.print("Creating new Auth file in 'out' folder.");
                Logger.print("Enter username:");
                string username = Console.ReadLine();
                Logger.print("Set expire date.");
                Logger.print("Enter year:");
                string year = Console.ReadLine();
                int yearInt = Int32.Parse(year);
                Logger.print("Enter month:");
                string month = Console.ReadLine();
                int monthInt = Int32.Parse(month);
                Logger.print("Enter day:");
                string day = Console.ReadLine();
                int dayInt = Int32.Parse(day);





                DateTime newDate = new DateTime(yearInt, monthInt, dayInt);
                long binaryNewDate = newDate.ToBinary();

                // Encrypt the string to an array of bytes.
                byte[] encrypted = AesControl.EncryptStringToBytes_Aes(username+","+binaryNewDate, key, IV);


                System.IO.Directory.CreateDirectory(directory + "\\out");

                // Decrypt the bytes to a string.
                System.IO.File.WriteAllBytes(directory + "\\out\\auth.txt", encrypted);
            }
            catch (Exception ex)
            {
                Logger.printError("Failed: " + ex.StackTrace);
            }
        }

    }
}
