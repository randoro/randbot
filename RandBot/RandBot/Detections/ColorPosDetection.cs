﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RandBot.Consequences;

namespace RandBot.Detections
{
    class ColorPosDetection : Detection
    {
        public override bool autoSet { get { return true; } }
        public Color detectionColor;
        public Point detectionPos;

        public ColorPosDetection(String detectionName, String detectionDescription, Consequence consequence, Color detectionColor, Point detectionPos)
        {
            this.detectionName = detectionName;
            this.detectionDescription = detectionDescription;
            this.consequence = consequence;
            this.detectionColor = detectionColor;
            this.detectionPos = detectionPos;

            notSame = 0;
        }

        
        public override void CreatePositive(Point mousePos)
        {
            pixelColor = detectionColor;
            pixelPoint = detectionPos;
            Logger.print(detectionName + " pixel automatically found at " + pixelPoint.X + "/" + pixelPoint.Y + " with color:" + pixelColor.R + "/" + pixelColor.G + "/" + pixelColor.B);

        }

        public override bool CheckSimilarity()
        {
            Color detectedColor = ScreenReader.GetPixelColor(pixelPoint.X, pixelPoint.Y);

            if (detectedColor.R == pixelColor.R && detectedColor.G == pixelColor.G && detectedColor.B == pixelColor.B)
            {
                Logger.printError("Detection failed! " + detectionName + " nr " + notSame + "/" + totalFail);
                Logger.print("Found color: " + detectedColor.R + "/" + detectedColor.G + "/" + detectedColor.B);
                Logger.print("Found color: " + pixelColor.R + "/" + pixelColor.G + "/" + pixelColor.B);
                Logger.print("Found location: " + pixelPoint.X + "/" + pixelPoint.Y);
                notSame++;
            }
            else
            {
                Logger.print("Found color: " + detectedColor.R + "/" + detectedColor.G + "/" + detectedColor.B);
                Logger.print("Found color: " + pixelColor.R + "/" + pixelColor.G + "/" + pixelColor.B);
                Logger.print("Found location: " + pixelPoint.X + "/" + pixelPoint.Y);
                notSame = 0;
                return true;
            }
            if (notSame > 2)
            {
                return false;
            }
            return true;
        }
    }
}
