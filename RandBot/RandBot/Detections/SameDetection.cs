﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RandBot.Consequences;

namespace RandBot.Detections
{
    class SameDetection : Detection
    {
        public override bool autoSet { get { return false; } }

        public SameDetection(String detectionName, String detectionDescription, Consequence consequence)
        {
            this.detectionName = detectionName;
            this.detectionDescription = detectionDescription;
            this.consequence = consequence;

            notSame = 0;
        }


        public override bool CheckSimilarity()
        {
            Color detectedColor = ScreenReader.GetPixelColor(pixelPoint.X, pixelPoint.Y);

            if (detectedColor.R == pixelColor.R && detectedColor.G == pixelColor.G && detectedColor.B == pixelColor.B)
            {
                notSame = 0;
                return true;
            }
            Logger.printError("Detection failed! " + detectionName + " nr " + notSame + "/" + totalFail);
            Logger.print("Found color: " + detectedColor.R + "/" + detectedColor.G + "/" + detectedColor.B);
            Logger.print("Found color: " + pixelColor.R + "/" + pixelColor.G + "/" + pixelColor.B);
            Logger.print("Found location: " + pixelPoint.X + "/" + pixelPoint.Y);
            notSame++;
            if (notSame > totalFail)
            {
                return false;
            }
            return true;
        }

    }
}
