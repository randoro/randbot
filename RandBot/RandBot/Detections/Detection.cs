﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using RandBot.Consequences;

namespace RandBot.Detections
{
    abstract class Detection
    {

        public Point pixelPoint;
        public Color pixelColor;
        public String detectionName;
        public String detectionDescription;
        public Consequence consequence;
        protected int totalFail = 5;
        protected int notSame;
        public abstract bool autoSet { get; }

        public abstract bool CheckSimilarity();


        public virtual void CreatePositive(Point mousePos)
        {
            pixelColor = ScreenReader.GetPixelColor(mousePos.X, mousePos.Y);
            pixelPoint = mousePos;
            Logger.print(detectionName + " pixel found at " + mousePos.X + "/" + mousePos.Y + " with color:" + pixelColor.R + "/" + pixelColor.G + "/" + pixelColor.B);
            
        }

        public void DescriptionHelp()
        {
            Logger.print("Set " + detectionName, ConsoleColor.White, ConsoleColor.DarkGreen);
            Logger.print("Mouse over and click '1' (Pixel perfect) on: " + detectionDescription);
        }

        public void Consequence()
        {
            Logger.print("Consequence event triggered on:" + detectionName);
            consequence.DoConsequence(this);
        }

    }
}
