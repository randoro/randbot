﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RandBot.Consequences;

namespace RandBot.Detections
{
    class FuzzyDetection : Detection
    {
        public override bool autoSet { get { return false; } }
        public int maxDifference;

        public FuzzyDetection(String detectionName, String detectionDescription, int maxDifference, Consequence consequence)
        {
            this.detectionName = detectionName;
            this.detectionDescription = detectionDescription;
            this.maxDifference = maxDifference;
            this.consequence = consequence;

            notSame = 0;
        }



        public override bool CheckSimilarity()
        {
            Color detectedColor = ScreenReader.GetPixelColor(pixelPoint.X, pixelPoint.Y);

            int differenceR = Math.Abs(detectedColor.R - pixelColor.R);
            int differenceG = Math.Abs(detectedColor.G - pixelColor.G);
            int differenceB = Math.Abs(detectedColor.B - pixelColor.B);


            if (differenceR <= maxDifference && differenceG <= maxDifference && differenceB <= maxDifference)
            {
                notSame = 0;
                return true;
            }
            Logger.printError("Detection failed! " + detectionName + " nr " + notSame + "/" + totalFail);
            Logger.print("Found color: " + detectedColor.R + "/" + detectedColor.G + "/" + detectedColor.B);
            Logger.print("Found color: " + pixelColor.R + "/" + pixelColor.G + "/" + pixelColor.B);
            notSame++;
            if (notSame > totalFail)
            {
                return false;
            }
            return true;
        }

    }
}
