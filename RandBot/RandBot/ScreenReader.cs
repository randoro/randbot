﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RandBot
{
    enum ShowWindowCommands
    {
        /// <summary>
        /// Hides the window and activates another window.
        /// </summary>
        Hide = 0,
        /// <summary>
        /// Activates and displays a window. If the window is minimized or 
        /// maximized, the system restores it to its original size and position.
        /// An application should specify this flag when displaying the window 
        /// for the first time.
        /// </summary>
        Normal = 1,
        /// <summary>
        /// Activates the window and displays it as a minimized window.
        /// </summary>
        ShowMinimized = 2,
        /// <summary>
        /// Maximizes the specified window.
        /// </summary>
        Maximize = 3, // is this the right value?
        /// <summary>
        /// Activates the window and displays it as a maximized window.
        /// </summary>       
        ShowMaximized = 3,
        /// <summary>
        /// Displays a window in its most recent size and position. This value 
        /// is similar to <see cref="Win32.ShowWindowCommand.Normal"/>, except 
        /// the window is not activated.
        /// </summary>
        ShowNoActivate = 4,
        /// <summary>
        /// Activates the window and displays it in its current size and position. 
        /// </summary>
        Show = 5,
        /// <summary>
        /// Minimizes the specified window and activates the next top-level 
        /// window in the Z order.
        /// </summary>
        Minimize = 6,
        /// <summary>
        /// Displays the window as a minimized window. This value is similar to
        /// <see cref="Win32.ShowWindowCommand.ShowMinimized"/>, except the 
        /// window is not activated.
        /// </summary>
        ShowMinNoActive = 7,
        /// <summary>
        /// Displays the window in its current size and position. This value is 
        /// similar to <see cref="Win32.ShowWindowCommand.Show"/>, except the 
        /// window is not activated.
        /// </summary>
        ShowNA = 8,
        /// <summary>
        /// Activates and displays the window. If the window is minimized or 
        /// maximized, the system restores it to its original size and position. 
        /// An application should specify this flag when restoring a minimized window.
        /// </summary>
        Restore = 9,
        /// <summary>
        /// Sets the show state based on the SW_* value specified in the 
        /// STARTUPINFO structure passed to the CreateProcess function by the 
        /// program that started the application.
        /// </summary>
        ShowDefault = 10,
        /// <summary>
        ///  <b>Windows 2000/XP:</b> Minimizes a window, even if the thread 
        /// that owns the window is not responding. This flag should only be 
        /// used when minimizing windows from a different thread.
        /// </summary>
        ForceMinimize = 11
    }

    static class ScreenReader
    {
        private static int count = 0;

        [DllImport("user32.dll")]
        static extern IntPtr GetDC(IntPtr hwnd);

        [DllImport("user32.dll")]
        static extern Int32 ReleaseDC(IntPtr hwnd, IntPtr hdc);

        [DllImport("gdi32.dll")]
        static extern uint GetPixel(IntPtr hdc, int nXPos, int nYPos);

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool GetWindowRect(IntPtr hWnd, out Rectangle lpRect);

        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, ShowWindowCommands nCmdShow);


        private static Bitmap map;
        private static bool mapped;

        //static public Color GetPixelColor(int x, int y)
        //{

        //    IntPtr hdc = GetDC(IntPtr.Zero);

        //    uint pixel = GetPixel(hdc, x, y);   

        //    ReleaseDC(IntPtr.Zero, hdc);
        //    Color color = Color.FromArgb((int)(pixel & 0x000000FF),
        //                 (int)(pixel & 0x0000FF00) >> 8,
        //                 (int)(pixel & 0x00FF0000) >> 16);
        //    return color;
        //}


        static public Color GetPixelColor(int x, int y)
        {
            if (!mapped)
            {
                RefreshScreenMap();
            }
            return map.GetPixel(x, y);
        }


        public static Point GetLineCross(int xPos, int yPos, int pathToCheck)
        {

            for (int i = 0; i < pathToCheck; i++)
            {
                Color c1 = map.GetPixel(xPos + i, yPos - i);
                Color c2 = map.GetPixel(xPos + i + 1, yPos - i);

                if (c1.R != 0 && c2.R != 0)
                    continue;

                if (c1.R == 0 && c1.G == 0 && c1.B == 0)
                {
                    //found black
                    Point p = new Point(xPos + i, yPos - i);
                    Logger.print("Found black at X:"+p.X+" y:"+p.Y);
                    return p;
                }

                if (c2.R == 0 && c2.G == 0 && c2.B == 0)
                {
                    //found black
                    Point p = new Point(xPos + i + 1, yPos - i);
                    Logger.print("Found black at X:" + p.X + " y:" + p.Y);
                    return p;
                }
            }

            return new Point(-1, -1);
        }

        public static bool CheckLineCrossCatch(int xPos, int yPos, int threshhold)
        {
            int pathToCheck = 100;

            for (int i = 0; i < pathToCheck; i++)
            {
                Color c1 = map.GetPixel(xPos, yPos - (pathToCheck/2) + i);

                if (c1.R != 0)
                    continue;

                if ((c1.R == 0 && c1.G == 0 && c1.B == 0))
                {
                    //found new line

                    int difference = yPos - (yPos - (pathToCheck/2) + i);
                    if (difference > threshhold || difference < -threshhold)
                    {
                        Logger.print("Found catch!");
                        return true;
                    }
                }
            }

            return false;
        }






        public static bool AuthWindow(string username)
        {
            foreach (Process pList in Process.GetProcesses())
            {
                if (pList.MainWindowTitle.StartsWith("Minecraft - CraftlandMod "))
                {
                    if (pList.MainWindowTitle.Equals("Minecraft - CraftlandMod " + username))
                    {
                        return true;
                    }
                    else
                    {
                        Logger.printError("The minecraft account you are currently using doesn't match the account of which your auth file holds.");
                        return false;
                    }
                }
            }

            Logger.printError("Can't find Minecraft/Craftland application, please run it before logging in.");
            return false;
        }




        public static bool FixWindowsPlacement()
        {
            IntPtr CLhWnd = IntPtr.Zero;
            foreach (Process pList in Process.GetProcesses())
            {
                if (pList.MainWindowTitle.StartsWith("Minecraft -"))
                {
                    CLhWnd = pList.MainWindowHandle;
                }
            }

            if (CLhWnd == IntPtr.Zero)
            {
                Logger.printError("Can't find Minecraft/Craftland application, please run it before starting the script.");
                return false;
            }



            IntPtr RBhWnd = IntPtr.Zero;
            foreach (Process pList in Process.GetProcesses())
            {
                if (pList.MainWindowTitle.StartsWith("Randbot"))
                {
                    RBhWnd = pList.MainWindowHandle;
                }
            }

            if (RBhWnd == IntPtr.Zero)
            {
                Logger.printError("Can't find Randbot, strange I wonder where this text is displayed then..");
                return false;
            }


            MoveWindow(RBhWnd, 1280, 0, 600, 600, true);

            ShowWindow(CLhWnd, ShowWindowCommands.Restore);
            ShowWindow(CLhWnd, ShowWindowCommands.Show);
            MoveWindow(CLhWnd, 0, 0, 1280, 800, true);
            SetForegroundWindow(CLhWnd);


            return true;

        }




        public static void RefreshScreenMap()
        {
            IntPtr hWnd = IntPtr.Zero;
            foreach (Process pList in Process.GetProcesses())
            {
                if (pList.MainWindowTitle.StartsWith("Minecraft -"))
                {
                    hWnd = pList.MainWindowHandle;
                }
            }

            if (hWnd == IntPtr.Zero)
            {
                Logger.printError("Can't find Minecraft/Craftland application, please run it before starting the script.");
            }


            Rectangle rect = new Rectangle();
            GetWindowRect(hWnd, out rect);
            
            //using (Bitmap bmpScreenCapture = new Bitmap(Screen.PrimaryScreen.Bounds.Width,
                                           // Screen.PrimaryScreen.Bounds.Height))

            //if (rect.X != -32000)
            //{
                map = new Bitmap(rect.Right, rect.Bottom);

                using (Graphics g = Graphics.FromImage(map))
                {
                    g.CopyFromScreen(0,
                        0,
                        0, 0,
                        map.Size,
                        CopyPixelOperation.SourceCopy);
                }


                mapped = true;
            //}

            //map.Save("test" + count + ".jpg", ImageFormat.Png);
            //count++;

        }


        

    }



    
}
