﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using Newtonsoft.Json;

namespace RandBot.Scripts
{
    static class ScriptControl
    {
        private static Thread scriptThread;
        private static object lockObject = new object();
        private static bool running;

        public static String[] scriptPaths;

        private static Script script;

        public static Point lineCross;


        public static void InitilizeScript()
        {

            scriptPaths = FileManager.LoadScriptsPaths();

            if (Program.globalSettings.currentScript)
            {
                //Script enabled
                script = FileManager.LoadScriptWithPath(Program.globalSettings.currentScriptPathAndName);
                Logger.print("Loaded script '"+Path.GetFileName(Program.globalSettings.currentScriptPathAndName)+"' from GlobalSettings.");
            }
            else
            {
                //No script enabled
                //load 0 element
                if (scriptPaths.Length > 0)
                {
                    Logger.print("Loaded first script (" + Path.GetFileName(scriptPaths[0]) + ") in script folder.");
                    script = FileManager.LoadScriptWithPath(scriptPaths[0]);
                    Program.globalSettings.currentScript = true;
                    Program.globalSettings.currentScriptPathAndName = scriptPaths[0];
                    FileManager.SaveGlobalSettings();
                }
                else
                {
                    Program.globalSettings.currentScript = false;
                    Logger.printError("Couldn't find any script to load.");
                    FileManager.SaveGlobalSettings();
                }
            }


            scriptThread = new Thread(ActivateScript);
            scriptThread.Start();
            running = false;
        }

        public static void ActivateScript()
        {
            Logger.print("Script Thread Started.");
            Thread.Sleep(1000);

            int index = 0;

            //LoadScripts();


            while (true)
            {
                lock (lockObject)
                {
                    CheckIfScriptShouldRun();

                    if (running)
                    {
                        if (!script.detectionSet)
                        {
                            script.SetDetection();
                        }


                        if (index > 2)
                        {
                            ScreenReader.RefreshScreenMap();
                            script.ValidateDetections();
                            index = 0;
                        }
                        index++;


                        if (!script.DoNextAction())
                        {
                            InputSimulator.SimulateKeyPress(VirtualKeyCode.NUMLOCK);
                        }

                    }
                }
            }
        }


        private static void CheckIfScriptShouldRun()
        {
            bool isControlDown = false;
            isControlDown = InputSimulator.IsKeyDown(VirtualKeyCode.CONTROL);

            bool isNumLockEnabled = false;
            isNumLockEnabled = InputSimulator.IsTogglingKeyInEffect(VirtualKeyCode.NUMLOCK);

            if (isNumLockEnabled)
            {
                if (running)
                {
                    running = false;
                    Logger.print("Script no longer running.");

                    if (isControlDown)
                    {
                        script.ResetScript();
                    }
                }
            }
            else
            {
                if (!running)
                {
                    running = true;
                    if (script.shouldResetOnPause)
                    {
                        script.ResetScript();
                    }
                    Logger.print("Script running.");

                    if (isControlDown)
                    {
                        script.ResetScript();
                    }
                }
            }
        }


        public static Script GetCurrentScript()
        {
            lock (lockObject)
            {
                return script;
            }
        }

        public static void SetCurrentScript(Script _script)
        {
            lock (lockObject)
            {
                script = _script;
            }
        }

        public static bool IsRunning()
        {
            return running;
        }

        


        //public static void DeactivateScript()
        //{
        //    //script = new Script();
        //    lock (lockObject)
        //    {
        //        script.ResetScript();
        //    }
        //    //script.ResetScript();
        //    //running = false;
        //    Logger.print("Script no longer running.");
        //}


    }
}
