﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using RandBot.Actions;
using RandBot.Consequences;
using RandBot.Detections;

namespace RandBot.Scripts
{
    class Script
    {
        public List<Detection> detectionList;
        public List<ScriptAction> actionList;
        private int index;
        public bool detectionSet;
        public bool shouldRepeat;
        public bool shouldResetOnPause;


        public Script()
        {
            actionList = new List<ScriptAction>();
            detectionList = new List<Detection>();
            index = 0;
            //detectionSet = false;

            //GoldNuggetScript();
            //GoldNuggetNotchScript();

        }



        public void GoldNuggetScript()
        {

            detectionList.Add(new SameDetection("PotatoSlot", "Middle of potato, not too close to the number of items.", new SortConsequence()));
            detectionList.Add(new SameDetection("SwordSlot", "Middle of sword, not too close to the number of items.", new SortConsequence()));

            detectionList.Add(new FuzzyDetection("Hungerbar", "Second last hungerbar in your hotbar.", 100, new EatConsequence()));




            actionList.Add(new KeyBoardAction(VirtualKeyCode.VK_7, 100));

            actionList.Add(new HoldAction(ClickType.Right, 5000, 100));


            actionList.Add(new KeyBoardAction(VirtualKeyCode.VK_3, 100));

            for (int i = 0; i < 10; i++)
            {
                actionList.Add(new MoveMouseAction(60, 0, 100));
            }

            for (int i = 0; i < 5; i++)
            {
                actionList.Add(new MoveMouseAction(0, -40, 100));
            }

            actionList.Add(new ClickAction(ClickType.Right, 100));

            for (int i = 0; i < 5; i++)
            {
                actionList.Add(new MoveMouseAction(0, 40, 100));
            }

            for (int i = 0; i < 10; i++)
            {
                actionList.Add(new MoveMouseAction(-60, 0, 100));
            }

            for (int i = 0; i < 1000; i++)
            {
                actionList.Add(new ClickAction(ClickType.Left, 100));
            }

            

        }


        private void GoldNuggetNotchScript()
        {

            detectionList.Add(new SameDetection("PotatoSlot", "Middle of potato, not too close to the number of items.", new SortConsequence()));

            detectionList.Add(new FuzzyDetection("Hungerbar", "Second last hungerbar in your hotbar.", 100, new EatConsequence()));




            actionList.Add(new KeyBoardAction(VirtualKeyCode.VK_7, 100));

            actionList.Add(new HoldAction(ClickType.Right, 5000, 100));


            actionList.Add(new KeyBoardAction(VirtualKeyCode.VK_3, 100));

            //for (int i = 0; i < 10; i++)
            //{
            //    actionList.Add(new MoveMouseAction(60, 0, 100));
            //}

            //for (int i = 0; i < 5; i++)
            //{
            //    actionList.Add(new MoveMouseAction(0, -40, 100));
            //}

            //actionList.Add(new ClickAction(Type.Right, 100));

            //for (int i = 0; i < 5; i++)
            //{
            //    actionList.Add(new MoveMouseAction(0, 40, 100));
            //}

            //for (int i = 0; i < 10; i++)
            //{
            //    actionList.Add(new MoveMouseAction(-60, 0, 100));
            //}

            for (int i = 0; i < 50; i++)
            {
                actionList.Add(new ClickAction(ClickType.Right, 4100));
            }



        }




        public void ValidateDetections()
        {
            foreach (Detection d in detectionList)
            {
                if (!d.CheckSimilarity())
                {
                    d.Consequence();
                }
            }
        }



        public bool DoNextAction()
        {
            if (actionList.Count > 0)
            {
                if (actionList.Count <= index)
                {
                    index = 0;

                    if (!shouldRepeat)
                    {
                        return false;
                    }

                    
                }

                actionList[index].DoAction();
                index++;

            }
            else
            {
                Logger.printError("Script is empty!");
                Thread.Sleep(100);
            }
            return true;
        }



        public void ResetScript()
        {
            index = 0;
        }


        public void ResetDetection()
        {
            detectionSet = false;
            SetDetection();
        }


        public void SetDetection()
        {
            ScreenReader.FixWindowsPlacement();
            ScreenReader.RefreshScreenMap();

            Logger.print("Setting detectionpoints.. use '1' on keyboard to set points using your mouse location.");
            foreach (Detection d in detectionList)
            {
                if (d.autoSet)
                {
                    ScreenReader.RefreshScreenMap();
                    d.CreatePositive(new Point(0,0));
                }
                else
                {
                d.DescriptionHelp();
                bool keystate1;
                bool set = false;
                    while (true)
                    {
                        keystate1 = InputSimulator.IsKeyDown(VirtualKeyCode.VK_1);
                        if ((!set && keystate1))
                        {
                            ScreenReader.RefreshScreenMap();
                            d.CreatePositive(FakeMouse.GetCursorPos());
                            set = true;
                        }

                        if (set && !keystate1)
                        {
                            break;
                        }
                    }
                }
            }
            detectionSet = true;
        }






    }
}
