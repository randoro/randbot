﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsInput;
using RandBot.Actions;
using RandBot.Consequences;
using RandBot.Detections;

namespace RandBot.Scripts
{
    static class ScriptGenerator
    {

        public static void GenerateScript()
        {
            if(!File.Exists(FileManager.BasePlusScriptDirectory + "\\GeneratorScript.json"))
            {


                Script script = new Script();

                script.shouldRepeat = true;
                script.shouldResetOnPause = false;

                script.detectionList.Add(new SameDetection("PotatoSlot",
                    "Middle of potato, not too close to the number of items.", new SortConsequence()));
                script.detectionList.Add(new FuzzyColorPosDetection("SwordSlot",
                    "Middle of sword, not too close to the number of items.", new DropSwordInHopperConsequence(),
                    Color.FromArgb(255, 207, 48, 0), new Point(505, 770), 30));

                script.detectionList.Add(new FuzzyDetection("Hungerbar", "Second last hungerbar in your hotbar.", 100,
                    new EatConsequence()));




                script.actionList.Add(new KeyBoardAction(VirtualKeyCode.VK_7, 100));

                script.actionList.Add(new HoldAction(ClickType.Right, 5000, 100));


                script.actionList.Add(new KeyBoardAction(VirtualKeyCode.VK_3, 100));

                for (int i = 0; i < 10; i++)
                {
                    script.actionList.Add(new MoveMouseAction(60, 0, 100));
                }

                for (int i = 0; i < 5; i++)
                {
                    script.actionList.Add(new MoveMouseAction(0, -40, 100));
                }

                script.actionList.Add(new ClickAction(ClickType.Right, 100));

                for (int i = 0; i < 5; i++)
                {
                    script.actionList.Add(new MoveMouseAction(0, 40, 100));
                }

                for (int i = 0; i < 10; i++)
                {
                    script.actionList.Add(new MoveMouseAction(-60, 0, 100));
                }

                for (int i = 0; i < 1000; i++)
                {
                    script.actionList.Add(new ClickAction(ClickType.Left, 100));
                }


                FileManager.SaveScript("GeneratorScript", script);
            }
        }

        public static void GenerateCraftScript()
        {
            if (!File.Exists(FileManager.BasePlusScriptDirectory + "\\CraftScript.json"))
            {


                Script script = new Script();
                script.shouldRepeat = false;
                script.shouldResetOnPause = true;

                //script.detectionList.Add(new SameDetection("PotatoSlot",
                //    "Middle of potato, not too close to the number of items.", new SortConsequence()));
                //script.detectionList.Add(new FuzzyColorPosDetection("SwordSlot",
                //    "Middle of sword, not too close to the number of items.", new DropSwordInHopperConsequence(),
                //    Color.FromArgb(255, 207, 48, 0), new Point(505, 770), 30));

                //script.detectionList.Add(new FuzzyDetection("Hungerbar", "Second last hungerbar in your hotbar.", 100,
                //    new EatConsequence()));




                script.actionList.Add(new ClickAction(ClickType.Right, 300));

                script.actionList.Add(new KeyBoardActionHold(VirtualKeyCode.SHIFT, 100));

                script.actionList.Add(new ClickLocationAction(ClickType.Left, new Point(780, 120), 100));

                script.actionList.Add(new KeyBoardTypeStringAction("gold ingot", 100));

                for (int i = 0; i < 20; i++)
                {

                    script.actionList.Add(new ClickLocationAction(ClickType.Left, new Point(780, 182), 20));
                    script.actionList.Add(new ClickLocationAction(ClickType.Left, new Point(780, 182), 20));

                    script.actionList.Add(new ClickLocationAction(ClickType.Left, new Point(580, 200), 20));

                }

                script.actionList.Add(new KeyBoardActionRelease(VirtualKeyCode.SHIFT, 100));

                script.actionList.Add(new KeyBoardAction(VirtualKeyCode.ESCAPE, 100));




                script.actionList.Add(new ClickAction(ClickType.Right, 300));

                script.actionList.Add(new KeyBoardActionHold(VirtualKeyCode.SHIFT, 100));

                script.actionList.Add(new ClickLocationAction(ClickType.Left, new Point(780, 120), 100));

                script.actionList.Add(new KeyBoardTypeStringAction("block of gold", 100));

                for (int i = 0; i < 20; i++)
                {

                    script.actionList.Add(new ClickLocationAction(ClickType.Left, new Point(780, 182), 10));
                    script.actionList.Add(new ClickLocationAction(ClickType.Left, new Point(780, 182), 10));

                    script.actionList.Add(new ClickLocationAction(ClickType.Left, new Point(580, 200), 10));

                }

                script.actionList.Add(new KeyBoardActionRelease(VirtualKeyCode.SHIFT, 100));

                script.actionList.Add(new KeyBoardAction(VirtualKeyCode.ESCAPE, 100));

                //script.actionList.Add(new ResetAndFinishAction(10));




                FileManager.SaveScript("CraftScript", script);
            }
        }


        public static void GenerateFishingScript()
        {

            if (!File.Exists(FileManager.BasePlusScriptDirectory + "\\FishingScript.json"))
            {

                Script script = new Script();
                script.shouldRepeat = true;
                script.shouldResetOnPause = true;

                script.detectionList.Add(new FuzzyColorPosDetection("FishingSlot",
                    "Durability part of fishing rod, left side", new DropFishingRodInHopperConsequence(),
                    Color.FromArgb(255, 207, 48, 0), new Point(625, 770), 30));

                //for (int i = 0; i < 15; i++)
                //{
                //    script.actionList.Add(new MoveMouseAction(0, 40, 100));
                //}

                //for (int i = 0; i < 13; i++)
                //{
                //    script.actionList.Add(new MoveMouseAction(0, -40, 100));
                //}

                //for (int i = 0; i < 100; i++)
                //{

                    script.actionList.Add(new KeyBoardAction(VirtualKeyCode.VK_5, 100));

                    script.actionList.Add(new ClickAction(ClickType.Right, 3000));

                    script.actionList.Add(new FindAndCreateLineCrossAction(250, 10));

                    script.actionList.Add(new CheckLineCrossAction(200, 10));

                //}

                FileManager.SaveScript("FishingScript", script);
            }
        }
    }
}
