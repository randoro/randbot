﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsInput;
using RandBot.Detections;

namespace RandBot.Consequences
{
    class StopConsequence : Consequence
    {

        public override void DoConsequence(Detection reCheck)
        {
            
            if (!InputSimulator.IsTogglingKeyInEffect(VirtualKeyCode.NUMLOCK))
            {
                InputSimulator.SimulateKeyPress(VirtualKeyCode.NUMLOCK);
            }
        }
    }
}
