﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsInput;
using RandBot.Actions;
using RandBot.Detections;

namespace RandBot.Consequences
{
    class SortConsequence : Consequence
    {
        public override void DoConsequence(Detection reCheck)
        {
            KeyBoardAction sortAction = new KeyBoardAction(VirtualKeyCode.VK_T, 100);

            sortAction.DoAction();

            ScreenReader.RefreshScreenMap();

            if (!reCheck.CheckSimilarity())
            {
                StopConsequence stopConsequence = new StopConsequence();
                stopConsequence.DoConsequence(reCheck);
            }
        }
    }
}
