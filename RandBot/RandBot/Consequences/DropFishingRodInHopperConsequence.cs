﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using RandBot.Actions;
using RandBot.Detections;
using RandBot.Scripts;

namespace RandBot.Consequences
{
    class DropFishingRodInHopperConsequence : Consequence
    {
        public override void DoConsequence(Detection reCheck)
        {
            MoveMouseAction moveDownAction = new MoveMouseAction(0, 40, 100);
            MoveMouseAction moveUpAction = new MoveMouseAction(0, -40, 100);
            KeyBoardAction swordAction = new KeyBoardAction(VirtualKeyCode.VK_5, 100);
            KeyBoardAction dropAction = new KeyBoardAction(VirtualKeyCode.SCROLL, 100);

            SortConsequence sortConsequence = new SortConsequence();

            for (int i = 0; i < 15; i++)
            {
                moveDownAction.DoAction();
            }

            swordAction.DoAction();
            dropAction.DoAction();

            Thread.Sleep(2000);

            for (int i = 0; i < 13; i++)
            {
                moveUpAction.DoAction();
            }

            sortConsequence.DoConsequence(reCheck);

            ScriptControl.GetCurrentScript().ResetScript();

            //eatAction.DoAction();
            //clickAction.DoAction();
            //swordAction.DoAction();

            //ScreenReader.RefreshScreenMap();

            //if (!reCheck.CheckSimilarity())
            //{
            //    StopConsequence stopConsequence = new StopConsequence();
            //    stopConsequence.DoConsequence(reCheck);
            //}
        }
    }
}
