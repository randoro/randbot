﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RandBot.Detections;

namespace RandBot.Consequences
{
    abstract class Consequence
    {

        public abstract void DoConsequence(Detection reCheck);
    }
}
