﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsInput;
using RandBot.Actions;
using RandBot.Detections;

namespace RandBot.Consequences
{
    class EatConsequence : Consequence
    {
        public override void DoConsequence(Detection reCheck)
        {
            KeyBoardAction eatAction = new KeyBoardAction(VirtualKeyCode.VK_7, 100);
            HoldAction clickAction = new HoldAction(ClickType.Right, 5000, 100);
            KeyBoardAction swordAction = new KeyBoardAction(VirtualKeyCode.VK_3, 100);

            eatAction.DoAction();
            clickAction.DoAction();
            swordAction.DoAction();

            ScreenReader.RefreshScreenMap();

            if (!reCheck.CheckSimilarity())
            {
                StopConsequence stopConsequence = new StopConsequence();
                stopConsequence.DoConsequence(reCheck);
            }
        }
    }
}
