﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RandBot
{
    static class FakeMouse
    {

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, UIntPtr dwExtraInfo);
        private const uint MOUSEEVENTF_LEFTDOWN = 0x02;
        private const uint MOUSEEVENTF_LEFTUP = 0x04;
        private const uint MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const uint MOUSEEVENTF_RIGHTUP = 0x10;


        //This is a replacement for Cursor.Position in WinForms
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetCursorPos(uint x, uint y);


        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetCursorPos(out Point lpPoint);




        public static Point GetCursorPos()
        {
            Point p;
            GetCursorPos(out p);
            return p;
        }

        public static void SetCursorPos(Point p)
        {
            SetCursorPos((uint)p.X, (uint)p.Y);
        }

        public static void SendMouseLeftClick()
        {
            Point p;
            GetCursorPos(out p);
            mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);
        }

        public static void SendSpecificMouseLeftClick(Point p)
        {
            mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);
        }

        public static void SendMouseRightClick()
        {
            Point p;
            GetCursorPos(out p);
            mouse_event(MOUSEEVENTF_RIGHTDOWN | MOUSEEVENTF_RIGHTUP, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);
        }

        public static void SendSpecificMouseRightClick(Point p)
        {
            mouse_event(MOUSEEVENTF_RIGHTDOWN | MOUSEEVENTF_RIGHTUP, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);
        }








        public static void SendMouseDoubleClick(Point p)
        {
            mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);

            Thread.Sleep(150);

            mouse_event(MOUSEEVENTF_LEFTDOWN | MOUSEEVENTF_LEFTUP, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);
        }

        public static void SendMouseRightDoubleClick(Point p)
        {
            mouse_event(MOUSEEVENTF_RIGHTDOWN | MOUSEEVENTF_RIGHTUP, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);

            Thread.Sleep(150);

            mouse_event(MOUSEEVENTF_RIGHTDOWN | MOUSEEVENTF_RIGHTUP, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);
        }

        //public static void SendMouseDown()
        //{
        //    mouse_event(MOUSEEVENTF_LEFTDOWN, 50, 50, 0, UIntPtr.Zero);
        //}

        //public static void SendMouseUp()
        //{
        //    mouse_event(MOUSEEVENTF_LEFTUP, 50, 50, 0, UIntPtr.Zero);
        //}


        public static void MoveMouse(int moveX, int moveY)
        {
            Point p;
            GetCursorPos(out p);
            SetCursorPos((uint)(p.X + moveX), (uint)(p.Y + moveY));

        }

        public static void SendMouseLeftHold(int time)
        {
            Point p;
            GetCursorPos(out p);

            mouse_event(MOUSEEVENTF_LEFTDOWN, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);
            Thread.Sleep(time);
            mouse_event(MOUSEEVENTF_LEFTUP, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);
        }

        public static void SendMouseRightHold(int time)
        {
            Point p;
            GetCursorPos(out p);

            mouse_event(MOUSEEVENTF_RIGHTDOWN, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);
            Thread.Sleep(time);
            mouse_event(MOUSEEVENTF_RIGHTUP, (uint)p.X, (uint)p.Y, 0, UIntPtr.Zero);


        }
    }
}
