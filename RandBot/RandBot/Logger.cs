﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandBot
{
    static class Logger
    {

        public static void print(String toPrint, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;
#if DEBUG
            Console.WriteLine(DateTime.Now.ToString("H:mm:ss") +" @"+memberName+ ": " + toPrint);
#else 
            Console.WriteLine(DateTime.Now.ToString("H:mm:ss") + ": " + toPrint);
#endif
        }


        
        public static void printError(String toPrint, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.Black;
#if DEBUG
            Console.WriteLine(DateTime.Now.ToString("H:mm:ss") + " @" + memberName + ":" + sourceFilePath + ":" + sourceLineNumber + " --- " + toPrint);
#else 
            Console.WriteLine(DateTime.Now.ToString("H:mm:ss") + ": " + toPrint);
#endif
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public static void print(String toPrint, ConsoleColor foregroundColor, ConsoleColor backgroundColor, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "",
        [System.Runtime.CompilerServices.CallerFilePath] string sourceFilePath = "",
        [System.Runtime.CompilerServices.CallerLineNumber] int sourceLineNumber = 0)
        {
            Console.BackgroundColor = backgroundColor;
            Console.ForegroundColor = foregroundColor;
#if DEBUG
            Console.WriteLine(DateTime.Now.ToString("H:mm:ss") + " @" + memberName + ":" + sourceFilePath + ":" + sourceLineNumber + " --- " + toPrint);
#else 
            Console.WriteLine(DateTime.Now.ToString("H:mm:ss") + ": " + toPrint);
#endif
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
